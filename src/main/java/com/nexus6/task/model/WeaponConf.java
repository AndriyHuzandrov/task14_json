package com.nexus6.task.model;

import com.nexus6.task.utils.TxtUtils;

public class WeaponConf {
  private int gunNum;
  private int machGunNum;

  public int getGunNum() {
    return gunNum;
  }

  public int getMachGunNum() {
    return machGunNum;
  }

  public void setGunNum(int gunNum) {
    this.gunNum = gunNum;
  }

  public void setMachGunNum(int machGunNum) {
    this.machGunNum = machGunNum;
  }

  @Override
  public String toString() {
    return String.format(TxtUtils.ARMAMENT_FORM, getMachGunNum(), getGunNum());
  }

}
