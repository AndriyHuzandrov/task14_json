package com.nexus6.task.model;

import com.nexus6.task.utils.TxtUtils;

public class Plane {
  private String model;
  private String cOfOrigin;
  private String type;
  private int ceiling;
  private WeaponConf wConf;
  private double length;
  private double height;
  private double width;
  private int price;

  public String getModel() {
    return model;
  }

  public String getcOfOrigin() {
    return cOfOrigin;
  }

  public String getType() {
    return type;
  }

  public WeaponConf getwConf() {
    return wConf;
  }

  public int getPrice() {
    return price;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public void setcOfOrigin(String cOfOrigin) {
    this.cOfOrigin = cOfOrigin;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setCeiling(int ceiling) {
    this.ceiling = ceiling;
  }

  public void setwConf(WeaponConf wConf) {
    this.wConf = wConf;
  }

  public void setLength(double length) {
    this.length = length;
  }

  public void setHeight(double height) {
    this.height = height;
  }

  public void setWidth(double width) {
    this.width = width;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  @Override
  public String toString() {
    return String.format(TxtUtils.PLANE_FROM, getModel(), getType(), getcOfOrigin(),
        getwConf().toString(), getPrice());
  }

}
