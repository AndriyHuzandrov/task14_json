package com.nexus6.task.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TxtUtils {

  public static final Logger appLog = LogManager.getLogger(TxtUtils.class);
  private static List<String> txtMenu;
  public static String CONS_INP_ERROR = "Error reading from console.";
  public static String BAD_MENU_ITM = "Bad menu item";
  public static String SEL_MENU_ITM = "Select menu item: ";
  public static String PARSE_ERR = "Fail to parse xml file";
  public static String TRANSF_ERR = "Transformation error";
  public static String PROP_FILE_PATH = "src/main/resources/config.properties";
  public static String PAUSE_MSG = "Press Enter to continue\n";
  public static String NO_FILE_ERR = "No such a file";
  public static String FILE_IO_ERR = "File Operation Error";
  public static String STAT_TAB_HEADER = "%30s%10s%n";
  public static String READ_OP = "Read operation";
  public static String TIME = "Time [s]";
  public static String START_B_READ = "Buffered reading from file\n";
  public static String STAT_TAB_ROW = "%30s%10.2f%n";
  public static String BAD_ARR_SIZE = "Bad buffer size. Setting up default value.";
  public static String SCHEMA_ERR = "Unable to create validation schema";
  public static String ARMAMENT_FORM = "Aircraft is armed with %d machine guns and %d gun%n";
  public static String PLANE_FROM = "Model: %s, aircraft class: %s%n" +
      "Produced in: %s%n%sEstimated cost: %d USD%n";
  public static int HEIGHT = 10;


  static {
    txtMenu = new LinkedList<>();
    txtMenu.add("1. Deserialize data from file.");
    txtMenu.add("0. Exit");
  }

  private TxtUtils() {
  }

  public static Supplier<Stream<String>> getTxtMenu() {
    return () -> txtMenu.stream();
  }
}