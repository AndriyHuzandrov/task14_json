package com.nexus6.task.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigHolder {
  private Properties config;

  public ConfigHolder() {
    config = new Properties();
    try(FileInputStream input = new FileInputStream(TxtUtils.PROP_FILE_PATH)) {
     config.load(input);
    } catch (IOException e) {
      TxtUtils.appLog.error(TxtUtils.FILE_IO_ERR);
    }
  }

  public String getParameter(String what) {
    return config.getProperty(what);
  }
}
