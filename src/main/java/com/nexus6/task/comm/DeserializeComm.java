package com.nexus6.task.comm;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nexus6.task.model.Plane;
import com.nexus6.task.utils.TxtUtils;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class DeserializeComm extends BlancCommand {
  public void execute() {
    Gson gson = new GsonBuilder().create();
    try(FileReader input = new FileReader(config.getParameter("planesFile"))) {
      Plane[] planesArr = gson.fromJson(input, Plane[].class);
      Arrays.stream(planesArr)
          .forEach(System.out::println);
    } catch (FileNotFoundException e) {
      TxtUtils.appLog.error(TxtUtils.NO_FILE_ERR);
    } catch (IOException e) {
      TxtUtils.appLog.error(TxtUtils.FILE_IO_ERR);
    }
  }
}
