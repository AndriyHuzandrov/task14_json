package com.nexus6.task.comm;

import com.nexus6.task.utils.ConfigHolder;
import java.util.Properties;

abstract class BlancCommand implements Executebale {
  ConfigHolder config;

  BlancCommand() {
    config = new ConfigHolder();
  }
  abstract public void execute();
}
